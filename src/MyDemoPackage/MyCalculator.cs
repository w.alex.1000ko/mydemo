﻿using System;

namespace MyDemoPackage
{
    public static class MyCalculator
    {
        public static int Add(int a, int b)
        {
            return a + b;
        }
    }
}